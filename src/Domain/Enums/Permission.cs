﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KaiShing.Domain.Enums
{
    public enum Permission
    {
        RoleManagement,
        UserManagement
    }
}
