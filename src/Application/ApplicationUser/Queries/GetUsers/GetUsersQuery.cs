﻿using KaiShing.Application.Common.Interfaces;
using KaiShing.Application.Common.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace KaiShing.Application.ApplicationUser.Queries.GetUsers
{
    public class GetUsersQuery : IRequest<Result>
    {
    }
    public class GetUsersQueryHandler : IRequestHandler<GetUsersQuery, Result>
    {
        private readonly IApplicationDbContext _context;
        private readonly IIdentityUserService _identityUserservice;

        public GetUsersQueryHandler(IApplicationDbContext context, IIdentityUserService identityUserservice)
        {
            _context = context;
            _identityUserservice = identityUserservice;
        }

        public async Task<Result> Handle(GetUsersQuery request, CancellationToken cancellationToken)
        {
            return await _identityUserservice.GetUsersAsync();
        }
    }
}
