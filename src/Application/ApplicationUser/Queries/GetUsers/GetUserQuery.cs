﻿using KaiShing.Application.Common.Interfaces;
using KaiShing.Application.Common.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace KaiShing.Application.ApplicationUser.Queries.GetUser
{
    public class GetUserQuery : IRequest<Result>
    {
        public string UserName { get; set; }
    }
    public class GetUserQueryHandler : IRequestHandler<GetUserQuery, Result>
    {
        private readonly IApplicationDbContext _context;
        private readonly IIdentityUserService _identityUserervice;

        public GetUserQueryHandler(IApplicationDbContext context, IIdentityUserService identityUserervice)
        {
            _context = context;
            _identityUserervice = identityUserervice;
        }

        public async Task<Result> Handle(GetUserQuery request, CancellationToken cancellationToken)
        {
            return await _identityUserervice.GetUserAsync(request.UserName);
        }
    }
}
