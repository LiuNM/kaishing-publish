﻿using KaiShing.Application.Common.Interfaces;
using KaiShing.Application.Common.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace KaiShing.Application.ApplicationUser.Commands.CreateUser
{

    public class CreateUsersCommand : IRequest<Result>
    {

        public string UserName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Name { get; set; }
    }

    public class CreateUserCommandHandler : IRequestHandler<CreateUsersCommand, Result>
    {

        private readonly IApplicationDbContext _context;
        private readonly IIdentityUserService _identityUserService;

        public CreateUserCommandHandler(IApplicationDbContext context, IIdentityUserService identityUserService)
        {
            _context = context;
            _identityUserService = identityUserService;
        }

        public async Task<Result> Handle(CreateUsersCommand request, CancellationToken cancellationToken)
        {

            return await _identityUserService.CreateUserAsync(request.UserName, request.Email, request.PhoneNumber, request.Name);
        }
    }
}
