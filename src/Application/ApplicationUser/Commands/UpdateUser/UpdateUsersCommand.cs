﻿using KaiShing.Application.Common.Interfaces;
using KaiShing.Application.Common.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace KaiShing.Application.ApplicationUser.Commands.UpdateUser
{

    public class UpdateUsersCommand : IRequest<Result>
    {
        public string UserName { get; set; }

    }

    public class UpdateUserCommandHandler : IRequestHandler<UpdateUsersCommand, Result>
    {

        private readonly IApplicationDbContext _context;
        private readonly IIdentityUserService _identityUserService;

        public UpdateUserCommandHandler(IApplicationDbContext context, IIdentityUserService identityUserService)
        {
            _context = context;
            _identityUserService = identityUserService;
        }

        public async Task<Result> Handle(UpdateUsersCommand request, CancellationToken cancellationToken)
        {
            return await _identityUserService.DeleteUserAsync(request.UserName);
        }
    }
}
