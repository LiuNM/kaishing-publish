﻿using KaiShing.Application.TodoLists.Queries.ExportTodos;
using System.Collections.Generic;

namespace KaiShing.Application.Common.Interfaces
{
    public interface ICsvFileBuilder
    {
        byte[] BuildTodoItemsFile(IEnumerable<TodoItemRecord> records);
    }
}
