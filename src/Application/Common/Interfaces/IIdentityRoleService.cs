﻿using KaiShing.Application.Common.Models;
using KaiShing.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace KaiShing.Application.Common.Interfaces
{
    public interface IIdentityRoleService
    {
        Task<Result> GetRoleAsync(string roleId);
        Task<Result> GetRolesAsync();
        Task<Result> CreateRoleAsync(string roleName, List<string> roleClaims, string roleDescription = "");
        Task<Result> UpdateRoleAsync(string roleName, List<string> roleClaims, string roleDescription = "");
        Task<Result> DeleteRoleAsync(string roleId);
        Task<Result> AssignAsync(string roleId,string[] usersName);
    }
}
