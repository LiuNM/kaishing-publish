﻿using System.Collections.Generic;
using System.Linq;

namespace KaiShing.Application.Common.Models
{
    public class Result
    {
        internal Result(bool succeeded, IEnumerable<string> errors, object content = null)
        {
            Succeeded = succeeded;
            Errors = errors.ToArray();
            Content = content;
        }

        public bool Succeeded { get; set; }
        public string[] Errors { get; set; }
        public object Content { get; set; }

        public static Result Success(object content = null)
        {
            return new Result(true, new string[] { }, content);
        }

        public static Result Failure(IEnumerable<string> errors)
        {
            return new Result(false, errors); 
        }

        public static Result Failure(string error)
        {
            return new Result(false, new string[] {error}); 
        }
    }
}
