﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KaiShing.Application.Common.Models
{
    public class UserDetail
    {
        public string UserName { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
    }
}
