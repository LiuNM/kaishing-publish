﻿using KaiShing.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;

namespace KaiShing.Application.Common.Models
{
    public class RoleDetail
    {
        public string RoleId { get; set; }
        public string RoleName { get; set; }
        public string RoleDescription { get; set; }
        public IList<Claim> RoleClaim { get; set; }
    }
}
