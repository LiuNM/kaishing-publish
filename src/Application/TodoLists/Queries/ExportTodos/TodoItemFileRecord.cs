﻿using KaiShing.Application.Common.Mappings;
using KaiShing.Domain.Entities;

namespace KaiShing.Application.TodoLists.Queries.ExportTodos
{
    public class TodoItemRecord : IMapFrom<TodoItem>
    {
        public string Title { get; set; }

        public bool Done { get; set; }
    }
}
