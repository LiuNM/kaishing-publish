﻿using KaiShing.Application.Common.Interfaces;
using KaiShing.Application.TodoItems.Commands.CreateTodoItem;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using KaiShing.Domain.Entities;
using KaiShing.Domain.Enums;
using KaiShing.Application.Common.Models;

namespace KaiShing.Application.Roles.Commadns.CreateRoles
{
    public class CreateRolesCommand : IRequest<Result>
    {

        public string RoleName { get; set; }
        public string RoleDescription { get; set; }
        public Permission[] RoleClaims { get; set; }
    }

    public class CreateRolesCommandHandler : IRequestHandler<CreateRolesCommand, Result>
    {
        
        private readonly IApplicationDbContext _context;
        private readonly IIdentityRoleService _identityRoleService;

        public CreateRolesCommandHandler(IApplicationDbContext context, IIdentityRoleService identityRoleService)
        {
            _context = context;
            _identityRoleService = identityRoleService;
        }

        public async Task<Result> Handle(CreateRolesCommand request, CancellationToken cancellationToken)
        {
            var claimsList = new List<string>();
            if(request.RoleClaims!=null)
            {

                foreach (var item in request.RoleClaims)
                {
                    claimsList.Add(item.ToString());
                }
            }
            return await _identityRoleService.CreateRoleAsync(request.RoleName, claimsList, request.RoleDescription);
             
        }
    }
}
