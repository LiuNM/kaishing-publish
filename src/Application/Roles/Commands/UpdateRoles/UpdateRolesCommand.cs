﻿using KaiShing.Application.Common.Interfaces;
using KaiShing.Application.TodoItems.Commands.CreateTodoItem;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using KaiShing.Domain.Entities;
using KaiShing.Domain.Enums;
using KaiShing.Application.Common.Models;

namespace KaiShing.Application.Roles.Commadns.UpdateRoles
{
    public class UpdateRolesCommand : IRequest<Result>
    {

        public string RoleId { get; set; }
        public string RoleName { get; set; }
        public string RoleDescription { get; set; }
        public Permission[] RoleClaims { get; set; }
    }


    public class UpdateRolesCommandHandler : IRequestHandler<UpdateRolesCommand, Result>
    {
        
        private readonly IApplicationDbContext _context;
        private readonly IIdentityRoleService _identityRoleService;

        public UpdateRolesCommandHandler(IApplicationDbContext context, IIdentityRoleService identityRoleService)
        {
            _context = context;
            _identityRoleService = identityRoleService;
        }

        public async Task<Result> Handle(UpdateRolesCommand request, CancellationToken cancellationToken)
        {
            var claimsList = new List<string>();
            foreach (var item in request.RoleClaims)
            {
                claimsList.Add(item.ToString());
            }
            return await _identityRoleService.UpdateRoleAsync(request.RoleName, claimsList, request.RoleDescription);
        }
    }
}
