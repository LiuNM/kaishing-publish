﻿using KaiShing.Application.Common.Interfaces;
using KaiShing.Application.Common.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace KaiShing.Application.Roles.Commands.UpdateRoles
{

    public class AssignUsersToRoseCommand : IRequest<Result>
    {
        public string RoleId { get; set; }
        public string[] UsersName { get; set; }
    }


    public class AssignUsersToRoseCommandHandler : IRequestHandler<AssignUsersToRoseCommand, Result>
    {

        private readonly IApplicationDbContext _context;
        private readonly IIdentityRoleService _identityRoleService;

        public AssignUsersToRoseCommandHandler(IApplicationDbContext context, IIdentityRoleService identityRoleService)
        {
            _context = context;
            _identityRoleService = identityRoleService;
        }

        public async Task<Result> Handle(AssignUsersToRoseCommand request, CancellationToken cancellationToken)
        {
            if (request.UsersName.Length > 0)
                return await _identityRoleService.AssignAsync(request.RoleId, request.UsersName);
            else
                return Result.Failure("There are no users to assign");
        }
    }
}
