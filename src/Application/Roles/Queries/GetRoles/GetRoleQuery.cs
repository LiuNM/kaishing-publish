﻿using KaiShing.Application.Common.Interfaces;
using KaiShing.Application.Common.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace KaiShing.Application.Roles.Quries.GetRoles
{

    public class GetRoleQuery : IRequest<Result>
    {
        public string RoleId { get; set; }
    }
    public class GetRoleQueryHandler : IRequestHandler<GetRoleQuery, Result>
    {
        private readonly IApplicationDbContext _context;
        private readonly IIdentityRoleService _identityRoleService;

        public GetRoleQueryHandler(IApplicationDbContext context, IIdentityRoleService identityRoleService)
        {
            _context = context;
            _identityRoleService = identityRoleService;
        }

        public async Task<Result> Handle(GetRoleQuery request, CancellationToken cancellationToken)
        {
            return await _identityRoleService.GetRoleAsync(request.RoleId);
        }
    }
}
