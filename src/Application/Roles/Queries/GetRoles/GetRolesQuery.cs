﻿using KaiShing.Application.Common.Interfaces;
using KaiShing.Application.Common.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace KaiShing.Application.Roles.Quries.GetRoles
{
    public class GetRolesQuery : IRequest<Result>
    {
    }
    public class GetRolesQueryHandler : IRequestHandler<GetRolesQuery, Result>
    {
        private readonly IApplicationDbContext _context;
        private readonly IIdentityRoleService _identityRoleService;

        public GetRolesQueryHandler(IApplicationDbContext context, IIdentityRoleService identityRoleService)
        {
            _context = context;
            _identityRoleService = identityRoleService;
        }

        public async Task<Result> Handle(GetRolesQuery request, CancellationToken cancellationToken)
        {
            return await _identityRoleService.GetRolesAsync();
        }
    }
}
