﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KaiShing.Application.Common.Models;
using KaiShing.Application.Roles.Commadns.CreateRoles;
using KaiShing.Application.Roles.Commadns.DeleteRoles;
using KaiShing.Application.Roles.Commadns.UpdateRoles;
using KaiShing.Application.Roles.Commands.UpdateRoles;
using KaiShing.Application.Roles.Quries.GetRoles;
using KaiShing.WebUI.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace WebUI.Controllers
{
    public class RolesController : ApiController
    {
        [HttpPost]
        public async Task<ActionResult<Result>> Create(CreateRolesCommand command)
        {
            return await Mediator.Send(command);
        }
        [HttpPut("id")]
        public async Task<ActionResult<Result>> Update(string id,UpdateRolesCommand command)
        {
            if (id != command.RoleId)
            {
                return BadRequest();
            }
            return await Mediator.Send(command);
        }
        [HttpGet("id")]
        public async Task<ActionResult<Result>> Get(string id)
        {
            return await Mediator.Send(new GetRoleQuery() { RoleId = id });
        }
        [HttpGet]
        public async Task<ActionResult<Result>> Get()
        {
            return await Mediator.Send(new GetRolesQuery());
        }
        [HttpDelete("id")]
        public async Task<ActionResult<Result>> Delete( string id)
        {
            return await Mediator.Send(new DeleteRolesCommand() {RoleId=id });
        }
        [HttpPost("id")]
        public async Task<ActionResult<Result>> Assign(string id, AssignUsersToRoseCommand command)
        {
            if (id != command.RoleId)
            {
                return BadRequest();
            }
            return await Mediator.Send(command);
        }
    }
}