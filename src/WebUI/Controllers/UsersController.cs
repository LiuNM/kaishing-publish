﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KaiShing.Application.ApplicationUser.Commands.CreateUser;
using KaiShing.Application.ApplicationUser.Commands.DeleteUser;
using KaiShing.Application.ApplicationUser.Commands.UpdateUser;
using KaiShing.Application.ApplicationUser.Queries.GetUser;
using KaiShing.Application.ApplicationUser.Queries.GetUsers;
using KaiShing.Application.Common.Models;
using KaiShing.WebUI.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebUI.Controllers
{

    public class UsersController : ApiController
    {
        [HttpPost]
        public async Task<Result> Create(CreateUsersCommand command)
        {
            return await Mediator.Send(command);
        }
        [HttpPut("{id}")]
        public async Task<ActionResult<Result>> Update(string id, UpdateUsersCommand command)
        {
            if (id != command.UserName)
            {
                return BadRequest();
            }
            return await Mediator.Send(command);
        }
        [HttpGet("{id}")]
        public async Task<ActionResult<Result>> Get(string id)
        {
            return await Mediator.Send(new GetUserQuery() { UserName = id });
        }
        [HttpGet]
        public async Task<ActionResult<Result>> Get()
        {
            return await Mediator.Send(new GetUsersQuery());
        }
        [HttpDelete("{id}")]
        public async Task<ActionResult<Result>> Delete(string id)
        {
            return await Mediator.Send(new DeleteUsersCommand() { UserName = id });
        }
    }
}