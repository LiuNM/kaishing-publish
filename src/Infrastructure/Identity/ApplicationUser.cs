﻿using Microsoft.AspNetCore.Identity;

namespace KaiShing.Infrastructure.Identity
{
    public class ApplicationUser : IdentityUser
    {
        public string StaffNo { get => UserName; set => UserName = value; }
        public string Name { get; set; }
    }
}
