﻿using KaiShing.Application.Common.Models;
using Microsoft.AspNetCore.Identity;
using System.Linq;

namespace KaiShing.Infrastructure.Identity
{
    public static class IdentityResultExtensions
    {
        public static Result ToApplicationResult(this IdentityResult result, string content = null)
        {
            return result.Succeeded
                ? Result.Success(content)
                : Result.Failure(result.Errors.Select(e => e.Description));
        }
    }
}