﻿using KaiShing.Domain.Common;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace KaiShing.Infrastructure.Identity
{
    public class Role : IdentityRole
    {
        public string RoleDescription { get; set; }
    }
}
