﻿using KaiShing.Application.Common.Interfaces;
using System;

namespace KaiShing.Infrastructure.Services
{
    public class DateTimeService : IDateTime
    {
        public DateTime Now => DateTime.Now;
    }
}
